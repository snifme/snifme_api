#! /bin/bash

echo "Migrating production database"
docker exec -it api1 script /dev/null -c "RAILS_ENV=production rake db:migrate"

