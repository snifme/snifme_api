Apipie.configure do |config|
  config.app_name                = "Snifme"
  config.api_base_url            = ""
  config.doc_base_url            = "/docs"
  config.validate                = false
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*controller.rb"
end
