require 'api_constraints'
Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  # Api definition
  namespace :api, defaults: { format: :json }, path: '/api'  do
    scope module: :v1,
          constraints: ApiConstraints.new(version: 1, default: true) do

      # We are going to list our resources here
      resources :users, only: [:show,:create,:update,:destroy]
      resources :sessions, only: [:create]
      delete 'sessions-delete' => 'sessions#destroy', as: 'sessions_delete'

      resources :logs, only: [:create]
      post 'log-session-token' => 'logs#get_session_token'
      patch 'merge-log-session-token' => 'logs#merge_session_token'

      resources :accounts, only: [:create]

      get 'confirm-account' => 'users#confirm_account'
      get 'resend-confirmation' => 'users#resend_confirmation'
      get 'password-reset-request' => 'users#password_reset_email'
      match 'password-reset' => 'users#password_reset', via: [:put,:patch]
      post 'facebook-connect' => 'sessions#facebook'
    end
  end
end
