require 'rails_helper'

RSpec.describe B2, type: :model do

  describe "#authorize" do

    let(:b2) { B2.new }

    context "authorization passes" do

      before do
        b2_stub_authorize_helper(b2)
        b2.authorize
      end

      it "sends out correct request" do
        expect(a_request(:get, b2_auth_url_helper)).to have_been_made.once
      end

      it "sets instance variables correctly when authenticates" do
        response = b2_auth_response_symbolized_helper
        expect(b2.authToken).to eq(response[:authorizationToken])
        expect(b2.apiUrl).to eq(response[:apiUrl])
        expect(b2.downloadUrl).to eq(response[:downloadUrl])
        expect(b2.authToken).to_not be_nil
        expect(b2.apiUrl).to_not be_nil
        expect(b2.downloadUrl).to_not be_nil
      end
    end

    context "authorization fails" do

      before do
        b2_stub_authorize_helper(b2, "fail")
        b2.authorize
      end

      it "does not set instance variables" do
        expect(b2.authToken).to be_nil
        expect(b2.apiUrl).to be_nil
        expect(b2.downloadUrl).to be_nil
      end
    end
  end

  describe "#get_upload_url" do

    let(:b2) { B2.new }
    let(:url) { B2::GET_UPLOAD_URL_PATH }

    context "when authentication failed" do

      before do
        b2_stub_authorize_helper(b2, "fail")
        b2.get_upload_url B2::POSTGRES_BUCKET_ID
      end

      it "does not call get_upload_url endpoint" do
        expect(a_request(:post, /#{url}/)).not_to have_been_made
      end

      it "does not set instance variables" do
        expect(b2.uploadAuthToken).to be_nil
        expect(b2.uploadUrl).to be_nil
      end

    end

    context "when authentication works but get_upload_url fails" do
      before do
        b2_stub_get_upload_url_helper(b2, "fail")
        b2.get_upload_url B2::POSTGRES_BUCKET_ID
      end

      it "does not call get_upload_url endpoint" do
        url = b2.apiUrl + B2::GET_UPLOAD_URL_PATH
        expect(a_request(:post, url)
          .with(:body => {"bucketId": B2::POSTGRES_BUCKET_ID}.to_json,
                :headers => {"Authorization" => b2.authToken}))
          .to(have_been_made.once)
      end

      it "does not set instance variables" do
        expect(b2.uploadAuthToken).to be_nil
        expect(b2.uploadUrl).to be_nil
      end
    end

    context "when authentication worked" do

      before do
        b2_stub_get_upload_url_helper(b2)
        b2.get_upload_url B2::POSTGRES_BUCKET_ID
      end

      it "calls get_upload_url endpoint with proper body and headers" do
        url = b2.apiUrl + B2::GET_UPLOAD_URL_PATH
        expect(a_request(:post, url)
          .with(:body => {"bucketId": B2::POSTGRES_BUCKET_ID}.to_json,
                :headers => {"Authorization" => b2.authToken}))
          .to(have_been_made.once)
      end

      it "sets the proper instance variables" do
        response = b2_get_upload_url_response_symbolized_helper b2
        expect(b2.uploadAuthToken).to eq(response[:authorizationToken])
        expect(b2.uploadUrl).to eq(response[:uploadUrl])
      end

    end
  end

  describe ".upload_file" do

    context "when get_upload_url fails" do

      before do
        b2 = B2.new
        b2_stub_get_upload_url_helper(b2, "fail")
      end

      it "should not make upload file request" do
        B2.upload_file B2::POSTGRES_BUCKET_ID, "fake", "fake"
        expect(a_request(:post, /https/)).to have_been_made.once
      end

      it "should return error" do
        expect(B2.upload_file(B2::POSTGRES_BUCKET_ID, "fake", "fake")).to(
        eq({"error" => "Could not get upload url"}))
      end
    end

    context "when upload_file fails" do

      before { b2_stub_upload_file_helper("fail") }

      it "should make upload file request" do
        B2.upload_file(B2::POSTGRES_BUCKET_ID, "tmp/backup_test.tar.gz", "application/x-gzip")
        url = "https://pod-000-1018-07.backblaze.com/b2api/v1/b2_upload_file/#{B2::POSTGRES_BUCKET_ID}/c001_v0001018_t0015"
        expect(a_request(:post, url)).to have_been_made.once
      end

      it "should return error" do
        expect(B2.upload_file(
          B2::POSTGRES_BUCKET_ID, "tmp/backup_test.tar.gz", "application/x-gzip"
        )[:error]).to(eq("Could not upload file"))
      end
    end

    context "when upload_file succeeds" do
      before { b2_stub_upload_file_helper }

      it "should make upload file request with proper body and headers" do
        B2.upload_file(B2::POSTGRES_BUCKET_ID, "tmp/backup_test.tar.gz", "application/x-gzip")
        url = "https://pod-000-1018-07.backblaze.com/b2api/v1/b2_upload_file/#{B2::POSTGRES_BUCKET_ID}/c001_v0001018_t0015"

        upload_info = b2_get_upload_url_response_symbolized_helper B2.new

        expect(a_request(:post, url).with(
          :headers => {
            "Authorization" => upload_info[:authorizationToken],
            "X-Bz-File-Name" => "tmp/backup_test",
            "Content-Type" => "application/x-gzip",
            "X-Bz-Content-Sha1" => "174e23326efca1725ca760bef3ec32a096f9fcf7",
            "Content-Length" => "7678"
          },
          :body => File.read("tmp/backup_test.tar.gz")
        )).to have_been_made.once
      end

      it "should return proper success response" do
        response = B2.upload_file(
          B2::POSTGRES_BUCKET_ID, "tmp/backup_test.tar.gz", "application/x-gzip")
        expect(response).to(eq(b2_upload_file_response_symbolized_helper))
        expect(response[:contentLength]).to eq(7678)
      end
    end
  end

  describe ".download_file_by_name" do

    it "should call authorize" do
      b2_test_that_authorize_is_called_during_download
    end

    context "when authentication fails" do
      it "should return proper error" do
        b2_test_that_download_returns_error_if_auth_fails
      end
      it "should not send download request" do
        b2_test_download_request_not_sent_if_auth_fails
      end
    end

    context "when authorization passes" do
      it "should send request with proper header and url" do
        b2_test_download_request_is_sent_with_proper_data_and_headers_helper
      end

      context "when download file fails" do
        it "should return error" do
          b2_test_download_request_fail_returns_error
        end
      end

      context "when download file succeeds" do

        context "when file corrupt" do
          it "should send proper error message" do
            b2_test_download_success_with_bad_sha1_returns_error
          end
        end

        context "when file good" do
          it "should send response with file" do
            b2_test_download_success_returns_proper_response
          end
        end
      end
    end
  end
end
