require 'rails_helper'

RSpec.describe Postgres, type: :model do

  describe ".backup" do
    it "should call proper log messages" do
      pg_test_backup_calls_log_messages_helper
    end

    it "should call system with proper command" do
      pg_test_backup_calls_system_with_proper_command_helper
    end
  end

  describe ".push_backup_to_b2" do

    context "file does not exist" do
      before { pg_stub_file_exist_helper "false" }

      it "should not call B2.upload_file" do
        pg_test_should_not_call_upload_file_helper
      end

      it "should log proper info with error" do
        pg_test_should_call_log_with_error_helper
      end
    end

    context "file exists" do

      before { pg_stub_file_exist_helper }

      it "should call B2.upload_file with proper params" do
        pg_test_push_backup_calls_proper_upload_method_helper
      end

      context "upload fails" do

        before { pg_stub_push_backup_helper "fail" }

        it "should log proper messages with error" do
          pg_test_log_error_when_push_fails_helper
        end
      end

      context "upload succeeds" do

        context "sha1 doesn't match" do

          before { pg_stub_push_backup_helper false, 'corrupt' }

          it "should log proper error" do
            pg_test_log_error_when_push_fails_helper
          end
        end

        context "sha1 matches" do
          before { pg_stub_push_backup_helper }

          it "should log proper info" do
            pg_test_push_backup_succeeds_logs_info_helper
          end

          it "should remove file" do
            pg_test_backup_file_removed_helper
          end

          it "should not have error log" do
            pg_test_backup_success_should_not_have_error_log
          end
        end
      end
    end
  end

  describe ".pull_backup_from_b2" do

    it "should call B2.download_file_by_name with proper arguments" do
      pg_test_should_call_download_file_by_name_helper
    end

    it "should call proper logger message" do
      pg_test_download_file_should_have_logger_message_helper
    end

    context "when download fails" do
      it "should not create file" do
        pg_test_download_fail_doesnt_create_file_helper
      end
      it "should call proper logger error" do
        pg_test_logger_error_when_download_fails_helper
      end
    end

    context "when download succeeds" do

      before { pg_delete_backup_file_if_exists_helper }
      after { pg_delete_backup_file_if_exists_helper }

      it "should call File open with correct attributes" do
        pg_test_should_call_file_open_helper
      end

      it "should create file from data" do
        pg_test_backup_file_should_be_created_helper
      end

      it "should call proper logger message" do
        pg_test_download_file_should_have_logger_message_helper
      end

    end
  end

  describe ".restore" do
    it "should call proper logger messages" do
      pg_test_restore_calls_proper_logger_messages_helper
    end
    it "should check to see if file exists" do
      pg_test_restore_checks_for_file_helper
    end
    context "when file doesn't exist" do
      it "should call proper logger error" do
        pg_test_restore_calls_error_when_no_file_helper
      end
      it "should not call restore command" do
        pg_test_restore_doesnt_call_restore_when_no_file_helper
      end
    end
    context "when file exists" do
      it "should unzip file" do
        pg_test_unzip_file_called_helper
      end
      it "should restore db" do
        pg_test_restore_called_helper
      end
      it "should remove db file" do
        pg_restore_file_removed_helper
      end
    end
  end

  describe ".backup_and_push_to_b2" do
    it "should call B2.backup" do
      pg_test_backup_and_push_calls_backup_helper
    end
    it "should call B2.push_backup_to_b2" do
      pg_test_backup_and_push_calls_push_backup_helper
    end
  end

  describe ".pull_from_b2_and_restore" do
    it "should call B2.pull_backup_from_b2" do
      pg_test_pull_and_restore_calls_pull_helper
    end
    it "should call B2.restore" do
      pg_test_pull_and_restore_calls_restore_helper
    end
  end

end

