module SessionTestHelpers

  CREATE_ERROR = {json: { errors: "Invalid email or password. If you registered"+
             " with Facebook and don't yet have a password, please use the"+
             " \"Connect With Facebook\" below to login." }, status: 422}

  def session_test_authenticate_returns_error_when_no_user_helper
    response = Session.authenticate_and_login(nil,nil)
    expect(response).to eq(CREATE_ERROR)
  end

  def session_test_authenticate_returns_error_when_no_password_digest_helper
    user = FactoryGirl.create(:user, password_digest: nil)
    response = Session.authenticate_and_login(user,nil)
    expect(response).to eq(CREATE_ERROR)
  end

  def session_test_authenticate_returns_error_when_password_wrong_helper
    user = FactoryGirl.create(:user,password: "q1w2e3r4",password_confirmation: "q1w2e3r4")
    response = Session.authenticate_and_login(user,"wrongpassword")
    expect(response).to eq(CREATE_ERROR)
  end

  def session_test_authenticate_calls_login_and_serialize_helper
    user = FactoryGirl.create(:user,password: "q1w2e3r4",password_confirmation: "q1w2e3r4")
    expect(user).to receive(:login_and_serialize)
    Session.authenticate_and_login(user,"q1w2e3r4")
  end

  def session_test_authenticate_doesnt_call_error_helper
    user = FactoryGirl.create(:user,password: "q1w2e3r4",password_confirmation: "q1w2e3r4")
    response = Session.authenticate_and_login(user,"q1w2e3r4")
    expect(response).not_to eq(CREATE_ERROR)
  end

  def session_test_authenticate_returns_correct_response_helper
    user = FactoryGirl.create(:user,password: "q1w2e3r4",password_confirmation: "q1w2e3r4")
    response = Session.authenticate_and_login(user,"q1w2e3r4")
    expect(response[:json][:token]).not_to be_nil
    expect(response[:status]).to eq(200)
  end

  def session_test_create_calls_authenticate_helper
    user = FactoryGirl.create(:user, password: "q1w2e3r4",
                              password_confirmation: "q1w2e3r4",
                              email: "test@gmail.com")
    params = {
      password: "q1w2e3r4",
      email: "test@gmail.com"
    }

    expect(Session).to receive(:authenticate_and_login).with(user,"q1w2e3r4")
    Session.create_action_handler params
  end

end
