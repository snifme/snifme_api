module B2TestHelper

  module AuthHelpers
    def b2_auth_token_helper
      b2_auth_response_symbolized_helper[:authorizationToken]
    end

    def b2_stub_auth_token_helper
      allow_any_instance_of(B2).to(
        receive(:authToken).and_return(b2_auth_token_helper))
    end

    def b2_download_url_helper
      b2_auth_response_symbolized_helper[:downloadUrl]
    end

    def b2_stub_download_url_helper
      allow_any_instance_of(B2).to(
        receive(:downloadUrl).and_return(b2_download_url_helper))
    end

    def b2_auth_url_helper
      "https://#{B2::ACCOUNT_ID}:#{B2::APP_KEY}@#{B2::AUTHORIZE_LINK}"
    end

    def b2_stub_authorize_helper b2, failed=false
      response = failed.present? ? b2_failed_auth_response_helper : b2_auth_response_helper
      stub_request(:get, b2_auth_url_helper).to_return(:body => response,
        :headers => {"Content-Type"=> "application/json"})
    end

    def b2_auth_response_helper
      {"accountId"=>B2::ACCOUNT_ID,
      "apiUrl"=>"https://api001.backblaze.com",
      "authorizationToken"=>"3_20160206212932_3abb027ae1e6d0f2f4b9ed91_669a95c5db8c9b04c2c31b3bb3cb231a0d09a090_001_acct",
      "downloadUrl"=>"https://f001.backblaze.com"}.to_json
    end

    def b2_auth_response_symbolized_helper
      JSON.parse(b2_auth_response_helper).deep_symbolize_keys!
    end

    def b2_failed_auth_response_helper
      {"code"=>"bad_auth_token", "message"=>"Invalid authorization token", "status"=>401}.to_json

    end
  end

  module GetUploadUrlHelpers
    def b2_get_upload_url_response_helper b2
      {"authorizationToken"=>"3_20160207004822_1d21e108a77a94a7a25efb18_9a426794bba8102232bbf301f7ba174cec41d7d4_001_upld",
       "bucketId"=> B2::POSTGRES_BUCKET_ID,
       "uploadUrl"=>"https://pod-000-1018-07.backblaze.com/b2api/v1/b2_upload_file/#{B2::POSTGRES_BUCKET_ID}/c001_v0001018_t0015"}.to_json
    end

    def b2_get_upload_url_response_symbolized_helper b2
      JSON.parse(b2_get_upload_url_response_helper(b2)).deep_symbolize_keys!
    end

    def b2_failed_get_upload_url_response_helper b2
      {"code"=>"bad_auth_token", "message"=>"Invalid authorization token", "status"=>401}.to_json
    end

    def b2_stub_get_upload_url_helper b2, failed=false
      b2_stub_authorize_helper(b2)

      if failed.present?
        response = b2_failed_get_upload_url_response_helper b2
      else
        response = b2_get_upload_url_response_helper b2
      end

      path = B2::GET_UPLOAD_URL_PATH

      stub_request(:post, /#{path}/).to_return(:body => response,
        :headers => {"Content-Type"=> "application/json"})
    end
  end

  module UploadFileHelpers
    def b2_upload_file_response_helper
      {"accountId"=>B2::ACCOUNT_ID,
       "bucketId"=>B2::POSTGRES_BUCKET_ID,
       "contentLength"=>7678,
       "contentSha1"=>"174e23326ea1725ca760bef3ec32a096f9fcf7",
       "contentType"=>"application/x-gzip",
       "fileId"=>"4_zedefa07f713b96ae57230f1d_f115ff518ae83411a_d20160207_m022640_c001_v0001005_t0033",
       "fileInfo"=>{},
       "fileName"=>"backup_test"}.to_json
    end

    def b2_upload_file_response_symbolized_helper
      JSON.parse(b2_upload_file_response_helper).deep_symbolize_keys!
    end

    def b2_failed_upload_file_response_helper
      {"error" => "Could not upload file"}.to_json
    end

    def b2_stub_upload_file_helper failed=false
      b2 = B2.new
      b2_stub_get_upload_url_helper b2

      if failed.present?
        response = b2_failed_upload_file_response_helper
      else
        response = b2_upload_file_response_helper
      end

      url = "https://pod-000-1018-07.backblaze.com/b2api/v1/b2_upload_file/#{B2::POSTGRES_BUCKET_ID}/c001_v0001018_t0015"

      stub_request(:post, url).to_return(:body => response,
        :headers => {"Content-Type"=> "application/json"})
    end
  end

  module DownloadFileHelpers

    def b2_prepare_download_file_helper failed=false, corrupt=false
      b2 = B2.new
      b2_stub_authorize_helper b2
      b2_stub_auth_token_helper
      b2_stub_download_url_helper
      b2_stub_download_file_helper failed, corrupt
    end

    def b2_test_download_request_fail_returns_error
      b2_prepare_download_file_helper 'fail'
      expect(call_download_file_by_name).to eq(
        JSON.parse(b2_failed_download_response_helper).deep_symbolize_keys!
      )
    end

    def b2_test_download_success_returns_proper_response
      b2_prepare_download_file_helper
      expect(call_download_file_by_name[:body]).to eq(
        b2_download_response_helper[:body]
      )
      expect(call_download_file_by_name[:headers]).to eq(
        b2_download_response_helper[:headers]
      )
      expect(call_download_file_by_name).to eq(
        b2_download_response_helper
      )
    end

    def b2_test_download_success_with_bad_sha1_returns_error
      b2_prepare_download_file_helper false, 'corrupt'
      expect(call_download_file_by_name).to eq(
        b2_download_corrupt_response_helper
      )
    end

    def b2_test_download_request_is_sent_with_proper_data_and_headers_helper
      b2_prepare_download_file_helper 'fail'
      call_download_file_by_name

      download_headers = {"Authorization" => b2_auth_token_helper}
      expect(a_request(:get, b2_download_url_full_helper).with(
        headers: download_headers
      )).to have_been_made.once
    end

    def b2_download_url_full_helper
      "#{b2_download_url_helper}/file/postgres-backups/tmp/backup_test.tar.gz"
    end

    def b2_test_download_request_not_sent_if_auth_fails
      allow_any_instance_of(B2).to receive(:authorize).and_return(nil)
      expect(a_request(:get, b2_download_url_full_helper)).not_to have_been_made
    end

    def call_download_file_by_name
      B2.download_file_by_name "postgres-backups", "tmp/backup_test.tar.gz"
    end

    def b2_test_that_download_returns_error_if_auth_fails
      allow_any_instance_of(B2).to receive(:authorize).and_return(nil)
      error = {status: 500, message: "B2 could not authenticate"}
      expect(call_download_file_by_name).to eq(error)
      expect(call_download_file_by_name).not_to eq("hello")
    end

    def b2_test_that_authorize_is_called_during_download
      expect_any_instance_of(B2).to receive(:authorize).once
      call_download_file_by_name
    end

    def b2_failed_download_auth_response_helper
      {status: 500, message: "B2 could not authenticate"}.to_json
    end

    def b2_failed_download_response_helper
      return {status: 500, message: "Download file failed"}.to_json
    end

    def b2_download_corrupt_response_helper
      return {status: 500, message: "file corrupt"}
    end

    def b2_download_response_helper corrupt=false
      sha1 = corrupt.present? ? "corruptsha1" : "174e23326efca1725ca760bef3ec32a096f9fcf7"
      response = {}
      response[:headers] = {"server"=>["Apache-Coyote/1.1"], "x-content-type-options"=>["nosniff"], "x-xss-protection"=>["1; mode=block"], "x-frame-options"=>["SAMEORIGIN"], "cache-control"=>["max-age=0, no-cache, no-store"], "x-bz-file-name"=>["tmp/backup_test"], "x-bz-file-id"=>["4_zedefa07f713b96ae57230f1d_f115ff518ae83411a_d20160207_m022640_c001_v0001005_t0033"], "x-bz-content-sha1"=>[sha1], "accept-ranges"=>["bytes"], "content-type"=>["application/x-gzip"], "content-length"=>["7678"], "date"=>["Mon, 08 Feb 2016 01:10:15 GMT"], "connection"=>["close"]}
      response[:body] = File.read("tmp/backup_test.tar.gz").force_encoding("ISO-8859-1")
      response
    end

    def b2_download_response_symbolized_helper
      JSON.parse(b2_download_response_helper).deep_symbolize_keys!
    end

    def b2_stub_download_file_helper failed=false, corrupt=false
      if failed.present?
        body = b2_failed_download_response_helper
        headers = {"Content-Type"=> "application/json"}
        status = 500
      else
        response = b2_download_response_helper corrupt
        headers = response[:headers]
        body = response[:body]
        status = 200
      end

      stub_request(:get, b2_download_url_full_helper).to_return(
        :body => body,
        :headers => headers,
        :status => status)
    end
  end
end
