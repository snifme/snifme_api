module FacebookTestHelper

  def facebook_params
    return {"age_range"=>{"min"=>21}, "context"=>{"mutual_likes"=>{"data"=>nil, "summary"=>{"total_count"=>100}}, "id"=>"dXNlcl9jb250ZAXh0OgGQSHpKtSiUEZAkoWkqbMNSp9SUdX44owc7ssHQ5chZBzAIqS8hMvXcj0UUdXEesI3sRW5frh44lwuWQZCZCyo0enLmQ7JbzjefGRJrsb28M2FVfZC0ZD"}, "devices"=>[{"os"=>"Android"}], "email"=>"montylennie@gmail.com", "first_name"=>"Monty", "gender"=>"male", "is_verified"=>false, "last_name"=>"Lennie", "link"=>"https://www.facebook.com/app_scoped_user_id/10153924513477952/", "name_format"=>"{first} {last}", "timezone"=>-8, "updated_time"=>"2015-09-27T05:03:46+0000", "verified"=>true, "cover"=>{"id"=>"10153828320827952", "offset_y"=>48, "source"=>"https://scontent.xx.fbcdn.net/hphotos-xta1/t31.0-0/p180x540/12273747_10153828320827952_5358892710279601551_o.jpg"}, "picture"=>{"data"=>{"is_silhouette"=>false, "url"=>"https://scontent.xx.fbcdn.net/hprofile-xpt1/v/t1.0-1/p50x50/12063294_10153722470197952_5236961478730015283_n.jpg?oh=1e8bf7b4fc1c2c0e3d95c06096818d11&oe=573D71BD"}}, "id"=>"10153924513477952", "token"=>"1234", "session"=>{"age_range"=>{"min"=>21}, "context"=>{"mutual_likes"=>{"data"=>nil, "summary"=>{"total_count"=>100}}, "id"=>"dXNlcl9jb250ZAXh0OgGQSHpKtSiUEZAkoWkqbMNSp9SUdX44owc7ssHQ5chZBzAIqS8hMvXcj0UUdXEesI3sRW5frh44lwuWQZCZCyo0enLmQ7JbzjefGRJrsb28M2FVfZC0ZD"}, "devices"=>[{"os"=>"Android"}], "email"=>"montylennie@gmail.com", "first_name"=>"Monty", "gender"=>"male", "is_verified"=>false, "last_name"=>"Lennie", "link"=>"https://www.facebook.com/app_scoped_user_id/10153924513477952/", "name_format"=>"{first} {last}", "timezone"=>-8, "updated_time"=>"2015-09-27T05:03:46+0000", "verified"=>true, "cover"=>{"id"=>"10153828320827952", "offset_y"=>48, "source"=>"https://scontent.xx.fbcdn.net/hphotos-xta1/t31.0-0/p180x540/12273747_10153828320827952_5358892710279601551_o.jpg"}, "picture"=>{"data"=>{"is_silhouette"=>false, "url"=>"https://scontent.xx.fbcdn.net/hprofile-xpt1/v/t1.0-1/p50x50/12063294_10153722470197952_5236961478730015283_n.jpg?oh=1e8bf7b4fc1c2c0e3d95c06096818d11&oe=573D71BD"}}, "id"=>"10153924513477952"}}.deep_symbolize_keys!
  end

  def serialized_facebook_params
    return {
     :age_range=>"{\"min\":21}",
     :devices=>"[{\"os\":\"Android\"}]",
     :email=>"montylennie@gmail.com",
     :first_name=>"Monty",
     :gender=>"male",
     :is_verified=>false,
     :last_name=>"Lennie",
     :link=>"https://www.facebook.com/app_scoped_user_id/10153924513477952/",
     :name_format=>"{first} {last}",
     :timezone=>-8,
     :updated_time=>"2015-09-27T05:03:46+0000",
     :verified=>true,
     :cover=>"{\"id\":\"10153828320827952\",\"offset_y\":48,\"source\":\"https://scontent.xx.fbcdn.net/hphotos-xta1/t31.0-0/p180x540/12273747_10153828320827952_5358892710279601551_o.jpg\"}",
     :picture=>"{\"is_silhouette\":false,\"url\":\"https://scontent.xx.fbcdn.net/hprofile-xpt1/v/t1.0-1/p50x50/12063294_10153722470197952_5236961478730015283_n.jpg?oh=1e8bf7b4fc1c2c0e3d95c06096818d11\\u0026oe=573D71BD\"}",
     :uid=>"10153924513477952",
     :token=>"1234"}
  end

  def fake_serialized_facebook_params
    return {
     :age_range=>"{\"min\":21}",
     :devices=>"[{\"os\":\"Android\"}]",
     :first_name=>"Monty",
     :gender=>"male",
     :is_verified=>false,
     :last_name=>"Lennie",
     :name_format=>"{first} {last}",
     :timezone=>-8,
     :updated_time=>"2015-09-27T05:03:46+0000",
     :verified=>true,
     :cover=>"{\"id\":\"10153828320827952\",\"offset_y\":48,\"source\":\"https://scontent.xx.fbcdn.net/hphotos-xta1/t31.0-0/p180x540/12273747_10153828320827952_5358892710279601551_o.jpg\"}",
     :picture=>"{\"is_silhouette\":false,\"url\":\"https://scontent.xx.fbcdn.net/hprofile-xpt1/v/t1.0-1/p50x50/12063294_10153722470197952_5236961478730015283_n.jpg?oh=1e8bf7b4fc1c2c0e3d95c06096818d11\\u0026oe=573D71BD\"}",
     :uid=>"10153924513477952"}
  end

end
