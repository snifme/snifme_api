module UserTestHelpers

  def user_test_serializes_user_accounts_helper
    user = FactoryGirl.create(:user)
    account = FactoryGirl.create(:account, user_id: user.id, kind: 0)
    response = user.serialize_accounts
    expected_data = {
      account_id: account.id,
      account_kind: "dog_owner"
    }
    expect(response[:accounts][0]).to eq(expected_data)
  end

  def user_test_is_dog_owner_true_helper
    user = FactoryGirl.create(:user)
    FactoryGirl.create(:account, kind: "dog_owner", user_id: user.id)
    expect(user.is_dog_owner?).to be true
  end

  def user_test_is_dog_owner_false_helper
    user = FactoryGirl.create(:user)
    expect(user.is_dog_owner?).to be false
  end

  def user_test_login_and_serialize_returns_token_helper
    user = FactoryGirl.create(:user)
    expect(Identity.count).to eq(0)
    response = user.login_and_serialize

    expect(Identity.count).to eq(1)
    expect(response[:token]).to eq(Identity.first.token)
  end

  def user_test_login_and_serialize_returns_user_info_helper
    user = FactoryGirl.create(:user)
    response = user.login_and_serialize
    expect(response[:user_email]).to eq(user.email)
    expect(response[:user_id]).to eq(user.id)
  end

  def user_test_login_and_serialize_returns_name_helper
    user = FactoryGirl.create(:user,first_name: "Monty", last_name: "Lennie")
    response = user.login_and_serialize
    expect(response[:first_name]).to eq("Monty")
    expect(response[:last_name]).to eq("Lennie")
  end

end
