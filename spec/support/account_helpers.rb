module AccountTestHelpers

  module CreateActionHandlerHelpers
    def account_test_create_action_handler_account_type_invalid_helper
      expect(Account).to receive(:error_response).with("invalid account type")
      expect(Account).not_to receive(:create_dog_owner)
      Account.create_action_handler({},{kind: "invalid"},{})
    end

    def account_test_create_action_handler_account_type_dog_owner_helper
      expect(Account).not_to receive(:error_response).with("invalid account type")
      param1 = {param: "one"}
      param2 = {kind: "dog_owner"}
      param3 = {param: "three"}
      expect(Account).to receive(:create_dog_owner).with(param1,param2,param3)
      Account.create_action_handler(param1,param2,param3)
    end
  end

  module CreateDogOwnerHelpers

    def account_test_create_dog_owner_already_owner_helper
      expect(Account).to receive(:error_response).with("already dog owner")
      expect_any_instance_of(User).not_to receive(:update)
      expect_any_instance_of(Account).not_to receive(:save)

      user = FactoryGirl.create(:user)
      FactoryGirl.create(:account,kind: "dog_owner",user_id: user.id)

      Account.create_dog_owner({},{kind: "dog_owner"}, user)
    end

    def account_test_create_dog_owner_user_name_update_helper
     user = FactoryGirl.create(:user, first_name: nil, last_name: nil)
     user_params = account_test_user_params_helper
     Account.create_dog_owner(user_params, {}, user)
     expect(user.reload.first_name).to eq("Monty")
     expect(user.reload.last_name).to eq("Lennie")
    end

    def account_test_user_params_helper
      {first_name: "Monty", last_name: "Lennie"}
    end

    def account_test_account_created_with_proper_info_helper
      user = FactoryGirl.create(:user)
      Account.create_dog_owner({},{kind: "dog_owner"}, user)
      account = Account.first
      expect(Account.all.count).to eq(1)
      expect(account.dog_owner?).to be true
    end

    def account_test_address_created_with_proper_info_helper
      user = FactoryGirl.create(:user)
      params = account_test_account_params_helper

      expect(Address.count).to eq(0)
      Account.create_dog_owner({}, params, user)
      expect(Address.count).to eq(1)

      address = Address.first
      expect(address.street1).to eq("1010 Howe street")
      expect(address.street2).to eq("buzzer 2093")
      expect(address.city).to eq("Vancouver")
      expect(address.state).to eq("BC")
      expect(address.country).to eq("Canada")
      expect(address.zipcode).to eq("v6z 1p5")
      expect(address.latitude).to eq("49.2797895")
      expect(address.longitude).to eq("-123.12420370000001")
      expect(address.geocoded_number).to eq("1010")
      expect(address.geocoded_street).to eq("Howe Street")
      expect(address.geocoded_city).to eq("Vancouver")
      expect(address.geocoded_state).to eq("British Columbia")
      expect(address.geocoded_state_short).to eq("BC")
      expect(address.geocoded_country_short).to eq("CA")
    end

    def account_test_account_params_helper
       {"kind"=>"dog_owner",
         "addresses_attributes"=>
          [{"street1"=>"1010 Howe street",
            "street2"=>"buzzer 2093",
            "city"=>"Vancouver",
            "state"=>"BC",
            "country"=>"Canada",
            "zipcode"=>"v6z 1p5",
            "latitude"=>49.2797895,
            "longitude"=>-123.12420370000001,
            "geocoded_number"=>"1010",
            "geocoded_street"=>"Howe Street",
            "geocoded_city"=>"Vancouver",
            "geocoded_country"=>"Canada",
            "geocoded_zipcode"=>"V6Z 1A8",
            "geocoded_state"=>"British Columbia",
            "geocoded_state_short"=>"BC",
            "geocoded_country_short"=>"CA"}]}
    end

    def account_test_create_dog_owner_serialize_accounts_called_helper
      user = FactoryGirl.create(:user)
      expect(user).to receive(:serialize_accounts)
      Account.create_dog_owner({},{kind: "dog_owner"},user)
    end

    def account_test_create_dog_owner_returns_proper_response_helper
      user = FactoryGirl.create(:user)
      user_params = account_test_user_params_helper
      account_params = account_test_account_params_helper
      response = Account.create_dog_owner(user_params, account_params, user)
      expected_response = { json: user.reload.serialize_accounts, status: 201 }
      expect(response).to eq(expected_response)
    end

    def account_test_returns_error_when_account_doesnt_save_helper
      user = FactoryGirl.create(:user)
      response = Account.create_dog_owner({},{},user)
      expect(response[:json][:errors][:kind]).to eq(["can't be blank"])
      expect(response[:json][:errors]["user.accounts"]).to eq(["is invalid"])
    end

  end

  def account_test_account_type_valid_helper
    response = Account.account_type_valid? "dog_owner"
    expect(response).to be true
  end

  def account_test_account_type_invalid_helper
    response = Account.account_type_valid? "something else"
    expect(response).to be false
  end

  def account_test_error_response_422_helper
    response = Account.error_response("not good")
    expect(response[:json]).to eq({errors: "not good"})
    expect(response[:status]).to eq(422)
  end

  def account_test_error_response_500_helper
    response = Account.error_response("not good", 500)
    expect(response[:json]).to eq({errors: "not good"})
    expect(response[:status]).to eq(500)
  end

end
