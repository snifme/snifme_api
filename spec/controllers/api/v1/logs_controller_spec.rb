require 'rails_helper'

RSpec.describe Api::V1::LogsController, type: :controller do

  describe "GET #get_session_token" do

    before do
      allow(Log).to receive(:generate_session_token) {"token1234"}
      get :get_session_token
    end

    it "returns the token" do
      expect(json_response[:token]).to eq("token1234")
    end

    it { should respond_with 200 }
  end

  describe "PATCH #merge_session_token" do

    context "when logs merge correctly" do
      before do
        expect(Log).to receive(:merge_logs_with_user).with("1","1234") {true}
        data = {user_id: 1,session_token: "1234"}
        patch :merge_session_token, data
      end
      it { should respond_with 204 }
    end

    context "when logs merge fails" do
      before do
        expect(Log).to receive(:merge_logs_with_user).with("1","1234") {false}
        data = {user_id: 1,session_token: "1234"}
        patch :merge_session_token, data
      end
      it { should respond_with 422 }
    end
  end

  describe "POST #create" do

    context "with valid data" do
      before do
        data = {
          log: {
            category: "Account",
            action: "create",
            label: "awesomeness",
            user_id: 3,
            session_token: "token1234"
          }
        }
        post :create, data
      end

      it "creates a log with proper data" do
        log = Log.first
        expect(log.category).to eq("Account")
        expect(log.action).to eq("create")
        expect(log.label).to eq("awesomeness")
        expect(log.user_id).to eq(3)
        expect(log.session_token).to eq("token1234")
        expect(Log.count).to eq(1)
      end

      it { should respond_with 200 }
    end

    context "with invalid data" do

      context "with no key" do
        it "raises an exception" do
          expect{ post(:create, {}) }.to raise_error ActionController::ParameterMissing
        end
      end

      context "with nil category" do

        before do
          post :create, {log: {label: nil}}
        end

        it "should not create a log" do
          expect(Log.count).to eq(0)
        end

        it { should respond_with 422 }
      end
    end
  end
end

