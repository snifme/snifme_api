FactoryGirl.define do

  factory :log do
    category "Account"
    action "Create"
    label "factories"
  end

end
