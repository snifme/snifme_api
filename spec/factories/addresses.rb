FactoryGirl.define do
  factory :address do
    name "MyString"
description "MyString"
street1 "MyString"
street2 "MyString"
city "MyString"
state "MyString"
zipcode "MyString"
zipcode_extra "MyString"
latitude "MyString"
longitude "MyString"
country "MyString"
extra_info "MyString"
concernable_type "MyString"
concernable_id 1
  end

end
