FactoryGirl.define do
  factory :facebook do
    age_range "MyString"
    devices "MyString"
    email "MyString"
    first_name "MyString"
    last_name "MyString"
    gender "MyString"
    is_verified false
    link "MyString"
    name_format "MyString"
    timezone 1
    updated_time "2016-01-20 04:08:29"
    verified false
    cover "MyString"
    picture "MyString"
    uid "MyString"
    archived false
  end
end
