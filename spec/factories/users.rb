FactoryGirl.define do
  factory :user do
    email { FFaker::Internet.email }
    password "q1w2e3r4"
    password_confirmation "q1w2e3r4"
  end

end
