require 'rails_helper'

RSpec.describe Account, type: :model do

  describe ".create_action_handler" do
    context "when account_type invalid" do
      it "should call and return error_response with error" do
        account_test_create_action_handler_account_type_invalid_helper
      end
    end
    context "when account_type valid" do
      context "account type is dog_owner" do
        it "should call and return create_dog_owner with proper params" do
          account_test_create_action_handler_account_type_dog_owner_helper
        end
      end
    end
  end

  describe ".create_dog_owner" do
    context "when user already dog owner" do
      it "should return error" do
        account_test_create_dog_owner_already_owner_helper
      end
    end
    context "when user not already dog owner" do
      context "when account saves" do
        it "should update user first and last name" do
          account_test_create_dog_owner_user_name_update_helper
        end
        it "should create account with proper info" do
          account_test_account_created_with_proper_info_helper
        end
        it "should create address with proper info" do
          account_test_address_created_with_proper_info_helper
        end
        it "should call user.serialize_accounts" do
          account_test_create_dog_owner_serialize_accounts_called_helper
        end
        it "should return object with proper json and status" do
          account_test_create_dog_owner_returns_proper_response_helper
        end
      end
      context "when account doesn't save" do
        it "should send return object with errors" do
          account_test_returns_error_when_account_doesnt_save_helper
        end
      end
    end

  end

  describe ".account_type_valid?" do
    it "should return true when dog_owner" do
      account_test_account_type_valid_helper
    end

    it "should return false when any thing else" do
      account_test_account_type_invalid_helper
    end
  end

  describe ".error_response" do
    context "with no status specified" do
      it "should return proper object with status 422" do
        account_test_error_response_422_helper
      end
    end
    context "with status specified" do
      it "shoudl return proper object with specified status" do
        account_test_error_response_500_helper
      end
    end
  end
end
