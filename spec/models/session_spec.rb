require 'rails_helper'

RSpec.describe Session, type: :model do

  describe ".authenticate_and_login" do
    context "when authentication fails" do
      it "should return error when user not present" do
        session_test_authenticate_returns_error_when_no_user_helper
      end
      it "should return error when password_digest not present" do
        session_test_authenticate_returns_error_when_no_password_digest_helper
      end
      it "should return error when user password wrong" do
        session_test_authenticate_returns_error_when_password_wrong_helper
      end
    end
    context "when authentication succeeds" do
      it "should not respond with error" do
        session_test_authenticate_doesnt_call_error_helper
      end
      it "should call login_and_serialize" do
        session_test_authenticate_calls_login_and_serialize_helper
      end
      it "should return correct response" do
        session_test_authenticate_returns_correct_response_helper
      end
    end
  end

  describe ".create_action_handler" do
    it "should call authenticate_and_login with proper params" do
      session_test_create_calls_authenticate_helper
    end
  end

end

