require 'rails_helper'

RSpec.describe Log, type: :model do

  describe "#generate_session_token" do
    it "should return a token" do
      expect(Log.generate_session_token).to be_truthy
    end
  end

  describe "#create_session_token_log" do
    it "should create a log with proper data" do
      token = Log.generate_session_token
      log = Log.create_session_token_log token
      expect(log.session_token).to eq(token)
      expect(log.category).to eq("Account")
      expect(log.action).to eq("Create")
      expect(log.label).to eq("Session Token")
      expect(Log.count).to eq(1)
    end
  end

  describe "#create_session_token" do
    it "should create a log with proper data and return the token" do
      token = Log.create_session_token
      log = Log.first
      expect(log.session_token).to eq(token)
      expect(log.category).to eq("Account")
      expect(log.action).to eq("Create")
      expect(log.label).to eq("Session Token")
      expect(Log.count).to eq(1)
    end
  end

  describe "#merge_logs_with_user" do

    let!(:user) {FactoryGirl.create(:user)}
    let!(:log_one) {FactoryGirl.create(:log, session_token: "1234")}
    let!(:log_two) {FactoryGirl.create(:log, session_token: "1234")}
    let!(:log_three) {FactoryGirl.create(:log, session_token: "different")}

    it "should merge the logs with matching session_token with the user" do
      expect(log_one.reload.user_id).to be_nil
      expect(log_two.reload.user_id).to be_nil
      Log.merge_logs_with_user(user.id, "1234")
      expect(log_one.reload.user_id).to eq(user.id)
      expect(log_two.reload.user_id).to eq(user.id)
    end

    it "should remove the session_tokens from the merged logs" do
      expect(log_one.reload.session_token).to_not be_nil
      expect(log_two.reload.session_token).to_not be_nil
      Log.merge_logs_with_user(user.id, "1234")
      expect(log_one.reload.session_token).to be_nil
      expect(log_two.reload.session_token).to be_nil
    end

    it "should not merge logs that don't match the session_token" do
      expect(log_three.reload.user_id).to be_nil
      expect(log_three.reload.session_token).to eq("different")
      Log.merge_logs_with_user(user.id, "1234")
      expect(log_three.reload.user_id).to be_nil
      expect(log_three.reload.session_token).to eq("different")
    end
  end
end
