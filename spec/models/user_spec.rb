require 'rails_helper'

RSpec.describe User, type: :model do

  describe "serialize_accounts" do
    it "should return user accounts with proper data" do
      user_test_serializes_user_accounts_helper
    end
  end

  describe "is_dog_owner?" do
    it "should return true when user is a dog owner" do
      user_test_is_dog_owner_true_helper
    end

    it "should return false when user is not a dog owner" do
      user_test_is_dog_owner_false_helper
    end

  end

  describe "login_and_serialize" do
    it "should create new Identity with user_id and add token to response" do
      user_test_login_and_serialize_returns_token_helper
    end
    it "should have user_id and user_email in response" do
      user_test_login_and_serialize_returns_user_info_helper
    end
    it "should have first and last name is response" do
      user_test_login_and_serialize_returns_name_helper
    end
  end

  describe "update_from_facebook" do
    context "when data has first last name" do
      context "when user first last name blank" do
        it "updates first and last name" do
          data = {first_name: "Monty", last_name: "Lennie"}
          user = FactoryGirl.create(:user, last_name: nil, first_name: nil)

          user.update_from_facebook(data)
          user.reload
          expect(user.first_name).to eq("Monty")
          expect(user.last_name).to eq("Lennie")
        end
      end
      context "when user first last name not blank" do
        it "updates first and last name" do
          data = {first_name: "Monty", last_name: "Lennie"}
          user = FactoryGirl.create(:user, last_name: "bill", first_name: "bob")

          user.update_from_facebook(data)
          user.reload
          expect(user.first_name).to eq("bob")
          expect(user.last_name).to eq("bill")
        end
      end
    end
    context "when data is blank" do
      context "when user first last name not" do
        it "does not update first and last name" do
          data = {}
          user = FactoryGirl.create(:user, last_name: "billy", first_name: "bob")

          user.update_from_facebook(data)
          user.reload
          expect(user.first_name).to eq("bob")
          expect(user.last_name).to eq("billy")
        end
      end
    end
  end
end
