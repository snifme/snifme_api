require 'rails_helper'

RSpec.describe Facebook, type: :model do

  let(:params) { facebook_params }

  describe "#prepare_data" do

    it "formats params correctly" do
      data = Facebook.prepare_data params
      expect(data).to eq(serialized_facebook_params)
      expect(data).to_not eq(fake_serialized_facebook_params)
    end
  end

  describe "#validate_fb_token" do
    let (:data) { serialized_facebook_params }
    before do
      allow(HTTParty).to receive(:get) {
        JSON.parse(
        {id: data[:uid], email: data[:email]}.to_json
      )}
    end

    context "when facebook graph response id matches data uid" do
      it "should return true" do
        expect(Facebook.validate_fb_token(data)).to be true
      end
    end

    context "when token is blank" do
      it "should return false" do
        data[:token] = nil
        expect(Facebook.validate_fb_token(data)).to be false
      end
    end

    context "when token is not present" do
      it "should return false" do
        data.delete(:token)
        expect(Facebook.validate_fb_token(data)).to be false
      end
    end

    context "when response id does not match data uid" do
      before do
        allow(HTTParty).to receive(:get) { JSON.parse(
          {id: "anotherId", email: data[:email]}.to_json
        )}
      end

      it "should return false" do
        expect(Facebook.validate_fb_token(data)).to be false
      end
    end

    context "when response email does not match data uid" do
      before do
        allow(HTTParty).to receive(:get) { JSON.parse(
          {id: data[:uid], email: "wrong@email.com"}.to_json
        )}
      end

      it "should return false" do
        expect(Facebook.validate_fb_token(data)).to be false
      end

    end

  end

  describe "#connect" do
    let (:data) { serialized_facebook_params }
    before do
      allow(HTTParty).to receive(:get) {
        JSON.parse({id: data[:uid],email: data[:email]}.to_json) }
    end

    context "when token validation fails" do
      before do
        allow(HTTParty).to receive(:get) { JSON.parse({id: "anotherid"}.to_json) }
      end

      it "should return 401 not authorized" do
        response = Facebook.connect data
        expect(response[:status]).to eq(401)
        expect(response[:errors]).to eq(["Not Authorized"])
      end

    end

    context "when facebook id or email is missing" do
      it "should return 422 with errors if data.uid is missing" do
        data[:uid] = nil
        response = Facebook.connect data
        expect(response[:status]).to eq(401)
        expect(response[:errors]).to eq(["Not Authorized"])
      end
      it "should return 422 with errors if data.email is missing" do
        data[:email] = nil
        response = Facebook.connect data
        expect(response[:status]).to eq(401)
        expect(response[:errors]).to eq(["Not Authorized"])
      end

    end

    context "when no user or facebook account exists" do
      it "should create a user,facebook and identity with token" do
        response = Facebook.connect data
        user = User.first
        expect(response[:status]).to eq(200)
        expect(User.count).to eq(1)
        expect(Facebook.count).to eq(1)
        expect(Identity.count).to eq(1)
        expect(User.first.facebook).to eq(Facebook.first)
        expect(User.first.first_name).to eq("Monty")
        expect(User.first.last_name).to eq("Lennie")
        expect(response[:json]).to eq(
          {token: User.first.reload.identities.first.token,
           user_id: user.id,
           user_email: user.email,
           first_name: user.first_name,
           last_name: user.last_name,
           fb_account_create: true,
           user_account_create: true,
          }
        )
        expect(user.identities.first.user_id).to eq(user.id)
      end

      it "should send a welcome email with proper info" do
        Facebook.connect data
        email = ActionMailer::Base.deliveries[0]
        expect(email.present?).to be true
        expect(email.from).to eq(["hello@snifme.com"])
        expect(email.to).to eq([User.first.email])
        expect(email.to).to eq([Facebook.first.email])
        expect(email.subject).to eq("Welcome to Snifme!")
      end

    end

    context "when user exists but facebook doesn't exist" do
      it "should create and associate facebook and Identity and send token" do
        user = FactoryGirl.create(:user,first_name: nil,last_name: nil)
        data[:email] = user.email
        response = Facebook.connect data
        expect(User.count).to eq(1)
        expect(user.reload.first_name).to eq("Monty")
        expect(user.reload.last_name).to eq("Lennie")
        expect(Facebook.count).to eq(1)
        expect(Identity.count).to eq(1)
        expect(user.facebook).to eq(Facebook.first)
        expect(response[:status]).to eq(200)
        expect(response[:json]).to eq(
          {token: User.first.reload.identities.first.token,
           user_id: user.id,
           user_email: user.email,
           first_name: user.first_name,
           last_name: user.last_name,
           fb_account_create: true,
          }
        )
        expect(response[:json][:user_account_create]).to be_nil
        expect(user.identities.first.user_id).to eq(user.id)
      end
    end

    context "when both user and facebook account already exists" do
      let(:user) { FactoryGirl.create(:user) }
      let(:fb) { FactoryGirl.create(:facebook, user_id: user.id) }
      before do
        data[:uid] = fb.uid
        data[:email] = user.email
        allow(HTTParty).to receive(:get) {
          JSON.parse({id: data[:uid],email: data[:email]}.to_json) }
      end

      it "should create Identity and send back token" do
        response = Facebook.connect data
        expect(User.count).to eq(1)
        expect(Facebook.count).to eq(1)
        expect(Identity.count).to eq(1)
        expect(user.identities.first.user_id).to eq(user.id)
        expect(user.facebook).to eq(Facebook.first)
        expect(response[:status]).to eq(200)
        expect(response[:json]).to eq(
          {token: User.first.reload.identities.first.token,
           user_id: user.id,
           user_email: user.email,
           first_name: user.first_name,
           last_name: user.last_name
          }
        )
        expect(response[:json][:user_account_create]).to be_nil
        expect(response[:json][:facebook_account_create]).to be_nil
      end
    end
  end

  describe "#create_account" do
    let(:user) { FactoryGirl.create(:user) }

    context "if facebook saves" do

      it "should return correct success response" do
        response = Facebook.create_account serialized_facebook_params, user
        expect(response[:status]).to eq(200)
        expect(response[:json]).to eq(
          {token: user.reload.identities.first.token,
           user_id: user.id,
           user_email: user.email,
           first_name: user.first_name,
           last_name: user.last_name,
           fb_account_create: true
          }
        )
        expect(user.identities.first.user_id).to eq(user.id)
      end

    end

    context "if facebook doesn't save" do

      it "should return correct fail response" do
        user.id = nil
        response = Facebook.create_account serialized_facebook_params, user

        expect(response[:status]).to eq(422)
        expect(
          response[:json][:errors].messages[:user_id][0]
        ).to eq("can't be blank")
      end

    end

  end

  describe "#prepare_data_and_connect" do
    before do
      allow(Facebook).to receive(:validate_fb_token) { true }
    end

    it "should prepare data and create user,facebook,identity and return token" do
      response = Facebook.prepare_data_and_connect params
      user = User.first
      expect(response[:status]).to eq(200)
      expect(User.count).to eq(1)
      expect(Facebook.count).to eq(1)
      expect(Identity.count).to eq(1)
      expect(User.first.facebook).to eq(Facebook.first)
      expect(response[:json]).to eq(
        {token: User.first.reload.identities.first.token,
         user_id: user.id,
         user_email: user.email,
         first_name: user.first_name,
         last_name: user.last_name,
         fb_account_create: true,
         user_account_create: true,
        }
      )
      expect(user.identities.first.user_id).to eq(user.id)
    end
  end
end
