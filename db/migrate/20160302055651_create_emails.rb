class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :name
      t.string :description
      t.integer :kind
      t.string :concernable_type
      t.integer :concernable_id
      t.string :email_address

      t.timestamps null: false
    end
    add_index :emails, :kind
    add_index :emails, :concernable_type
    add_index :emails, :concernable_id
  end
end
