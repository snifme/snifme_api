class CreateDogs < ActiveRecord::Migration
  def change
    create_table :dogs do |t|
      t.string :name
      t.string :description
      t.string :breed
      t.string :color
      t.datetime :birth_date
      t.integer :birth_address_id
      t.integer :sex
      t.integer :account_id

      t.timestamps null: false
    end
    add_index :dogs, :breed
    add_index :dogs, :color
    add_index :dogs, :birth_address_id
    add_index :dogs, :sex
    add_index :dogs, :account_id
  end
end
