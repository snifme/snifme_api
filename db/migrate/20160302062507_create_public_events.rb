class CreatePublicEvents < ActiveRecord::Migration
  def change
    create_table :public_events do |t|
      t.integer :kind
      t.string :name
      t.string :description
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps null: false
    end
    add_index :public_events, :kind
  end
end
