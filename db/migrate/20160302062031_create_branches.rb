class CreateBranches < ActiveRecord::Migration
  def change
    create_table :branches do |t|
      t.integer :establishment_id
      t.integer :kind
      t.string :location_instructions

      t.timestamps null: false
    end
    add_index :branches, :establishment_id
    add_index :branches, :kind
  end
end
