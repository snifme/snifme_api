class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :name
      t.string :description
      t.string :street1
      t.string :street2
      t.string :city
      t.string :state
      t.string :zipcode
      t.string :zipcode_extra
      t.string :latitude
      t.string :longitude
      t.string :geocoded_number
      t.string :geocoded_street
      t.string :geocoded_city
      t.string :geocoded_state
      t.string :geocoded_state_short
      t.string :geocoded_country
      t.string :geocoded_country_short
      t.string :geocoded_zipcode
      t.string :country
      t.string :extra_info
      t.string :concernable_type
      t.integer :concernable_id

      t.timestamps null: false
    end
    add_index :addresses, :concernable_type
    add_index :addresses, :concernable_id
    add_index :addresses, :geocoded_city
    add_index :addresses, :geocoded_country
    add_index :addresses, :geocoded_country_short
    add_index :addresses, :geocoded_state
    add_index :addresses, :geocoded_state_short
    add_index :addresses, :geocoded_zipcode
    add_index :addresses, :geocoded_street
  end
end
