class CreateEstablishments < ActiveRecord::Migration
  def change
    create_table :establishments do |t|
      t.integer :kind
      t.string :name
      t.text :description
      t.date :creation_date

      t.timestamps null: false
    end
    add_index :establishments, :kind
  end
end
