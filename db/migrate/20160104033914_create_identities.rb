class CreateIdentities < ActiveRecord::Migration
  def change
    create_table :identities do |t|
      t.string :provider
      t.integer :user_id
      t.datetime :expiry_date
      t.string :token
      t.boolean :archived, default: false

      t.timestamps null: false
    end
    add_index :identities, :user_id
    add_index :identities, :token
  end
end
