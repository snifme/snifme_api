class CreateRelationshipEvents < ActiveRecord::Migration
  def change
    create_table :relationship_events do |t|
      t.integer :relationship_id
      t.integer :action
      t.integer :relationship_side_id

      t.timestamps null: false
    end
    add_index :relationship_events, :relationship_id
    add_index :relationship_events, :action
    add_index :relationship_events, :relationship_side_id
  end
end
