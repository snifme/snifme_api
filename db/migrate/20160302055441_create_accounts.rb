class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.integer :user_id
      t.integer :establishment_id
      t.integer :branch_id
      t.integer :kind

      t.timestamps null: false
    end
    add_index :accounts, :user_id
    add_index :accounts, :establishment_id
    add_index :accounts, :branch_id
    add_index :accounts, :kind
  end
end
