class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :kind
      t.boolean :blood_related
      t.integer :family_category
      t.integer :current_status
      t.datetime :accepted_at

      t.timestamps null: false
    end
    add_index :relationships, :kind
    add_index :relationships, :family_category
    add_index :relationships, :current_status
  end
end
