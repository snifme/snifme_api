class CreatePhoneNumbers < ActiveRecord::Migration
  def change
    create_table :phone_numbers do |t|
      t.string :name
      t.integer :kind
      t.string :country_code
      t.string :area_code
      t.string :number
      t.string :extension
      t.string :concernable_type
      t.integer :concernable_id

      t.timestamps null: false
    end
    add_index :phone_numbers, :kind
    add_index :phone_numbers, :concernable_type
    add_index :phone_numbers, :concernable_id
  end
end
