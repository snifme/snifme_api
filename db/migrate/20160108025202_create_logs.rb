class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :category
      t.string :action
      t.string :label
      t.string :platform
      t.string :device_info
      t.string :languages
      t.string :language
      t.string :session_token
      t.text :description
      t.integer :user_id
      t.integer :itemable_id
      t.string :itemable_type
      t.string :error_name
      t.boolean :archived, default: false

      t.timestamps null: false
    end
    add_index :logs, :category
    add_index :logs, :action
    add_index :logs, :label
    add_index :logs, :session_token
    add_index :logs, :user_id
    add_index :logs, :itemable_id
    add_index :logs, :itemable_type
    add_index :logs, :error_name
  end
end
