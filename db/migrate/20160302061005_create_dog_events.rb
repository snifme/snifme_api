class CreateDogEvents < ActiveRecord::Migration
  def change
    create_table :dog_events do |t|
      t.integer :kind
      t.string :name
      t.string :description
      t.datetime :event_time
      t.integer :event_address_id
      t.integer :dog_id

      t.timestamps null: false
    end
    add_index :dog_events, :kind
    add_index :dog_events, :event_address_id
    add_index :dog_events, :dog_id
  end
end
