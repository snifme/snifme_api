class CreateRelationshipSides < ActiveRecord::Migration
  def change
    create_table :relationship_sides do |t|
      t.integer :dog_id
      t.string :title
      t.boolean :did_initiate
      t.boolean :following_other_dog
      t.integer :relationship_id

      t.timestamps null: false
    end
    add_index :relationship_sides, :dog_id
    add_index :relationship_sides, :relationship_id
  end
end
