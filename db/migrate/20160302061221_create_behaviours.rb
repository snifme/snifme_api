class CreateBehaviours < ActiveRecord::Migration
  def change
    create_table :behaviours do |t|
      t.string :title
      t.string :description
      t.datetime :date_started
      t.string :known_cause
      t.string :dealing_with
      t.integer :dog_id

      t.timestamps null: false
    end
    add_index :behaviours, :dog_id
  end
end
