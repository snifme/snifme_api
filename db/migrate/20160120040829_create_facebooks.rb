class CreateFacebooks < ActiveRecord::Migration
  def change
    create_table :facebooks do |t|
      t.string :age_range
      t.string :devices
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :gender
      t.boolean :is_verified
      t.string :link
      t.string :name_format
      t.integer :timezone
      t.datetime :updated_time
      t.boolean :verified
      t.string :cover
      t.string :picture
      t.string :uid
      t.integer :user_id, unique: true
      t.boolean :archived, default: false

      t.timestamps null: false
    end
    add_index :facebooks, :email
    add_index :facebooks, :uid
    add_index :facebooks, :user_id, unique: true
  end
end
