# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160302070046) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "establishment_id"
    t.integer  "branch_id"
    t.integer  "kind"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "accounts", ["branch_id"], name: "index_accounts_on_branch_id", using: :btree
  add_index "accounts", ["establishment_id"], name: "index_accounts_on_establishment_id", using: :btree
  add_index "accounts", ["kind"], name: "index_accounts_on_kind", using: :btree
  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", using: :btree

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",                   null: false
    t.string   "resource_type",                 null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.boolean  "archived",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "addresses", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "street1"
    t.string   "street2"
    t.string   "city"
    t.string   "state"
    t.string   "zipcode"
    t.string   "zipcode_extra"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "geocoded_number"
    t.string   "geocoded_street"
    t.string   "geocoded_city"
    t.string   "geocoded_state"
    t.string   "geocoded_state_short"
    t.string   "geocoded_country"
    t.string   "geocoded_country_short"
    t.string   "geocoded_zipcode"
    t.string   "country"
    t.string   "extra_info"
    t.string   "concernable_type"
    t.integer  "concernable_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "addresses", ["concernable_id"], name: "index_addresses_on_concernable_id", using: :btree
  add_index "addresses", ["concernable_type"], name: "index_addresses_on_concernable_type", using: :btree
  add_index "addresses", ["geocoded_city"], name: "index_addresses_on_geocoded_city", using: :btree
  add_index "addresses", ["geocoded_country"], name: "index_addresses_on_geocoded_country", using: :btree
  add_index "addresses", ["geocoded_country_short"], name: "index_addresses_on_geocoded_country_short", using: :btree
  add_index "addresses", ["geocoded_state"], name: "index_addresses_on_geocoded_state", using: :btree
  add_index "addresses", ["geocoded_state_short"], name: "index_addresses_on_geocoded_state_short", using: :btree
  add_index "addresses", ["geocoded_street"], name: "index_addresses_on_geocoded_street", using: :btree
  add_index "addresses", ["geocoded_zipcode"], name: "index_addresses_on_geocoded_zipcode", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "archived",               default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "behaviours", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "date_started"
    t.string   "known_cause"
    t.string   "dealing_with"
    t.integer  "dog_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "behaviours", ["dog_id"], name: "index_behaviours_on_dog_id", using: :btree

  create_table "branches", force: :cascade do |t|
    t.integer  "establishment_id"
    t.integer  "kind"
    t.string   "location_instructions"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "branches", ["establishment_id"], name: "index_branches_on_establishment_id", using: :btree
  add_index "branches", ["kind"], name: "index_branches_on_kind", using: :btree

  create_table "dog_events", force: :cascade do |t|
    t.integer  "kind"
    t.string   "name"
    t.string   "description"
    t.datetime "event_time"
    t.integer  "event_address_id"
    t.integer  "dog_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "dog_events", ["dog_id"], name: "index_dog_events_on_dog_id", using: :btree
  add_index "dog_events", ["event_address_id"], name: "index_dog_events_on_event_address_id", using: :btree
  add_index "dog_events", ["kind"], name: "index_dog_events_on_kind", using: :btree

  create_table "dogs", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "breed"
    t.string   "color"
    t.datetime "birth_date"
    t.integer  "birth_address_id"
    t.integer  "sex"
    t.integer  "account_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "dogs", ["account_id"], name: "index_dogs_on_account_id", using: :btree
  add_index "dogs", ["birth_address_id"], name: "index_dogs_on_birth_address_id", using: :btree
  add_index "dogs", ["breed"], name: "index_dogs_on_breed", using: :btree
  add_index "dogs", ["color"], name: "index_dogs_on_color", using: :btree
  add_index "dogs", ["sex"], name: "index_dogs_on_sex", using: :btree

  create_table "emails", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "kind"
    t.string   "concernable_type"
    t.integer  "concernable_id"
    t.string   "email_address"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "emails", ["concernable_id"], name: "index_emails_on_concernable_id", using: :btree
  add_index "emails", ["concernable_type"], name: "index_emails_on_concernable_type", using: :btree
  add_index "emails", ["kind"], name: "index_emails_on_kind", using: :btree

  create_table "establishments", force: :cascade do |t|
    t.integer  "kind"
    t.string   "name"
    t.text     "description"
    t.date     "creation_date"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "establishments", ["kind"], name: "index_establishments_on_kind", using: :btree

  create_table "facebooks", force: :cascade do |t|
    t.string   "age_range"
    t.string   "devices"
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "gender"
    t.boolean  "is_verified"
    t.string   "link"
    t.string   "name_format"
    t.integer  "timezone"
    t.datetime "updated_time"
    t.boolean  "verified"
    t.string   "cover"
    t.string   "picture"
    t.string   "uid"
    t.integer  "user_id"
    t.boolean  "archived",     default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "facebooks", ["email"], name: "index_facebooks_on_email", using: :btree
  add_index "facebooks", ["uid"], name: "index_facebooks_on_uid", using: :btree
  add_index "facebooks", ["user_id"], name: "index_facebooks_on_user_id", unique: true, using: :btree

  create_table "hours_of_operations", force: :cascade do |t|
    t.string   "monday"
    t.string   "tuesday"
    t.string   "wednesday"
    t.string   "thursday"
    t.string   "friday"
    t.string   "saturday"
    t.string   "sunday"
    t.string   "description"
    t.string   "exceptions"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "identities", force: :cascade do |t|
    t.string   "provider"
    t.integer  "user_id"
    t.datetime "expiry_date"
    t.string   "token"
    t.boolean  "archived",    default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "identities", ["token"], name: "index_identities_on_token", using: :btree
  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "logs", force: :cascade do |t|
    t.string   "category"
    t.string   "action"
    t.string   "label"
    t.string   "platform"
    t.string   "device_info"
    t.string   "languages"
    t.string   "language"
    t.string   "session_token"
    t.text     "description"
    t.integer  "user_id"
    t.integer  "itemable_id"
    t.string   "itemable_type"
    t.string   "error_name"
    t.boolean  "archived",      default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "logs", ["action"], name: "index_logs_on_action", using: :btree
  add_index "logs", ["category"], name: "index_logs_on_category", using: :btree
  add_index "logs", ["error_name"], name: "index_logs_on_error_name", using: :btree
  add_index "logs", ["itemable_id"], name: "index_logs_on_itemable_id", using: :btree
  add_index "logs", ["itemable_type"], name: "index_logs_on_itemable_type", using: :btree
  add_index "logs", ["label"], name: "index_logs_on_label", using: :btree
  add_index "logs", ["session_token"], name: "index_logs_on_session_token", using: :btree
  add_index "logs", ["user_id"], name: "index_logs_on_user_id", using: :btree

  create_table "phone_numbers", force: :cascade do |t|
    t.string   "name"
    t.integer  "kind"
    t.string   "country_code"
    t.string   "area_code"
    t.string   "number"
    t.string   "extension"
    t.string   "concernable_type"
    t.integer  "concernable_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "phone_numbers", ["concernable_id"], name: "index_phone_numbers_on_concernable_id", using: :btree
  add_index "phone_numbers", ["concernable_type"], name: "index_phone_numbers_on_concernable_type", using: :btree
  add_index "phone_numbers", ["kind"], name: "index_phone_numbers_on_kind", using: :btree

  create_table "public_events", force: :cascade do |t|
    t.integer  "kind"
    t.string   "name"
    t.string   "description"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "public_events", ["kind"], name: "index_public_events_on_kind", using: :btree

  create_table "relationship_events", force: :cascade do |t|
    t.integer  "relationship_id"
    t.integer  "action"
    t.integer  "relationship_side_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "relationship_events", ["action"], name: "index_relationship_events_on_action", using: :btree
  add_index "relationship_events", ["relationship_id"], name: "index_relationship_events_on_relationship_id", using: :btree
  add_index "relationship_events", ["relationship_side_id"], name: "index_relationship_events_on_relationship_side_id", using: :btree

  create_table "relationship_sides", force: :cascade do |t|
    t.integer  "dog_id"
    t.string   "title"
    t.boolean  "did_initiate"
    t.boolean  "following_other_dog"
    t.integer  "relationship_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "relationship_sides", ["dog_id"], name: "index_relationship_sides_on_dog_id", using: :btree
  add_index "relationship_sides", ["relationship_id"], name: "index_relationship_sides_on_relationship_id", using: :btree

  create_table "relationships", force: :cascade do |t|
    t.integer  "kind"
    t.boolean  "blood_related"
    t.integer  "family_category"
    t.integer  "current_status"
    t.datetime "accepted_at"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "relationships", ["current_status"], name: "index_relationships_on_current_status", using: :btree
  add_index "relationships", ["family_category"], name: "index_relationships_on_family_category", using: :btree
  add_index "relationships", ["kind"], name: "index_relationships_on_kind", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                                  null: false
    t.string   "password_digest"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "timezone"
    t.boolean  "archived",               default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end
