unless Rails.env.production?
  require 'rails_helper'

  class Authentication
    include Authenticable
    def request
      request
    end
    def render json
      json
    end
  end

  describe Authenticable, :type => :controller do
    let(:authentication) { Authentication.new }

    describe "#current_user" do
      before do
        @user = FactoryGirl.create :user
        request.headers["Authorization"] = @user.auth_token
        allow(authentication).to receive(:request).and_return(request)
      end

      it "returns the user from the authorization header" do
        expect(authentication.current_user.auth_token).to eql @user.auth_token
      end
    end

    describe '.authenticate_with_token' do
      context 'current_user is nil' do
        before do
          allow(authentication).to receive(:current_user).and_return(nil)
        end

        it 'returns error' do
          expect(authentication.authenticate_with_token![:json][:errors]).to eq 'Not authenticated'
        end

        it 'returns unauthorized status' do
          expect(authentication.authenticate_with_token![:status]).to eq :unauthorized
        end
      end

      context 'current_user is not nil' do
        before do
          allow(authentication).to receive(:current_user).and_return("present")
        end
        it 'should not return anything' do
          expect(authentication.authenticate_with_token!).to be_falsy
          expect(authentication.authenticate_with_token!).to be_falsy
        end
      end
    end

    describe "#user_signed_in?" do
      let!(:user) {FactoryGirl.create :user}

      context "when there is a user on 'session'" do
        before do
          allow(authentication).to receive(:current_user).and_return(user)
        end
        it("user signed in to be true") do
          expect(authentication.user_signed_in?).to be true
        end
      end

      context "when there is no user on 'session'" do
        before do
          allow(authentication).to receive(:current_user).and_return(nil)
        end

        it("user signed in to be false") do
          expect(authentication.user_signed_in?).to be false
        end
      end
    end
  end
end
