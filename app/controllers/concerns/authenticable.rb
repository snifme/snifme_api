module Authenticable
  # Devise methods overwrites
  def current_user
    token = request.headers['Authentication']
    return @current_user = nil unless token.present?
    uid = Identity.find_by(token: token).user_id;
    return @current_user = nil unless uid.present?
    @current_user ||= User.find(uid)
  end

  def authenticate_with_token!
    render json: { errors: "Not authenticated" },
                status: :unauthorized unless user_signed_in?
  end

  def user_signed_in?
    current_user.present?
  end
end
