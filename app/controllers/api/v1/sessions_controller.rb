class Api::V1::SessionsController < ApplicationController

  def create
    render Session.create_action_handler params
  end

  def destroy
    identity = Identity.find_by(token: params[:token])
    identity.destroy
    head 204
  end

  def facebook
    render Facebook.prepare_data_and_connect(params)
  end

end

