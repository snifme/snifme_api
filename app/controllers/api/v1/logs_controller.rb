class Api::V1::LogsController < ApplicationController

  def create
    log = Log.new(log_params)
    if log.save
      head 200
    else
      head 422
    end
  end

  def get_session_token
    render json: {token: Log.create_session_token}, status: 200
  end

  def merge_session_token
    if Log.merge_logs_with_user params[:user_id], params[:session_token]
      head 204
    else
      head 422
    end
  end

  private

    def log_params
      params.require(:log).permit(:category,:action,:label,:error_name,
                                  :user_id,:session_token, :platform, :device_info,
                                  :languages, :language
                                 )
    end

end

