unless Rails.env.production?
  require 'rails_helper'

  RSpec.shared_examples "not authenticated" do
    it 'should return a not authenticated error' do
      user_response = json_response
      expect(user_response[:errors]).to eq 'Not authenticated'
    end
    it { should respond_with 401 }
  end

  RSpec.describe Api::V1::UsersController, type: :controller do
    skip

      describe "GET #show" do
        before(:each) do
          @user = FactoryGirl.create :user
          get :show, id: @user.id
        end

        it "returns the information about a reporter on a hash" do
          user_response = json_response
          expect(user_response[:email]).to eql @user.email
          expect(user_response[:id]).to eql @user.id
          expect(user_response[:created_at]).to eql @user.created_at.iso8601(3)
          expect(user_response[:updated_at]).to eql @user.updated_at.iso8601(3)
        end

        it "does not return password" do
          user_response = json_response
          expect(user_response[:password]).to be_nil
        end

        it { should respond_with 200 }
      end

      describe "POST #create" do

        context "when is successfully created" do
          before(:each) do
            @user_attributes = FactoryGirl.attributes_for :user
            post :create, { user: @user_attributes }
          end

          it "renders the json representation for the user record just created" do
            user_response = json_response
            expect(user_response[:id]).to be_an(Integer)
            expect(user_response[:email]).to eql @user_attributes[:email]
            expect(user_response[:created_at]).to be_truthy
            expect(user_response[:updated_at]).to be_truthy
            expect(response.location).to be_truthy
            expect(user_response[:password]).to be_falsy
          end

          it { should respond_with 201 }
        end

        context "when is not created" do
          context "when email is missing" do
            before(:each) do
              #notice I'm not including the email
              @invalid_user_attributes = { password: "12345678",
                                           password_confirmation: "12345678" }
              post :create, { user: @invalid_user_attributes }
            end

            it "renders an errors json" do
              user_response = json_response
              expect(user_response).to have_key(:errors)
            end

            it "renders the json errors on why the user could not be created" do
              user_response = json_response
              expect(user_response[:errors][:email]).to include "can't be blank"
            end

            it { should respond_with 422 }
          end
          context "when passwords don't match" do
            before(:each) do
              @invalid_user_attributes = { email: "test@gmail.com",
                                           password: "123456544",
                                           password_confirmation: "12345678" }
              post :create, { user: @invalid_user_attributes }
            end

            it "renders an errors json" do
              user_response = json_response
              expect(user_response).to have_key(:errors)
            end

            it "renders the json errors on why the user could not be created" do
              user_response = json_response
              expect(user_response[:errors][:password_confirmation]
                    ).to include "doesn't match Password"
            end

            it { should respond_with 422 }
          end
        end
    end

    describe "PUT/PATCH #update" do
      let!(:user) {FactoryGirl.create(:user)}

      context "when not updated" do

        context "without a token" do
          before(:each) do
            patch :update, { id: user.id,
                             user: { email: "newmail@example.com" }
                           },
                           format: :json
          end
          it_behaves_like "not authenticated"
        end

        context 'has token but doesn\'t update due to invalid data' do
          before(:each) do
            api_authorization_header(user.auth_token)
            patch :update, { id: user.id,
                             user: { email: "invalid" }
                           },
                           format: :json
          end

          it "renders an errors json" do
            user_response = json_response
            expect(user_response).to have_key(:errors)
          end

          it "renders the json errors on why the user could not be created" do
            user_response = json_response
            expect(user_response[:errors][:email]).to include "is invalid"
          end

          it { should respond_with 422 }
        end
      end


      context "when is successfully updated" do

        before(:each) do
          api_authorization_header(user.auth_token)
          patch :update, { id: user.id,
                           user: { email: "newmail@example.com" } }
        end

        it "renders the json representation for the updated user" do
          user_response = json_response
          expect(user_response[:email]).to eql "newmail@example.com"
          expect(user_response[:id]).to eql(user.id)
          expect(user_response[:created_at]).to eql user.created_at.iso8601(3)
          expect(user_response[:updated_at]).to_not eql user.updated_at.iso8601(3)
          expect(user_response[:updated_at]).to be_truthy
          expect(response.location).to eq(
                 'http://api.test.host/users/' + user.id.to_s)
          expect(user_response[:password]).to be_falsy
        end

        it { should respond_with 200 }
      end

    end

    describe "DELETE #destroy" do
      let!(:user) {FactoryGirl.create(:user)}

      context "when user not authorized" do
        before(:each) do
          delete :destroy, { id: user.id }
        end
        it_behaves_like "not authenticated"
      end

      context "when user is authorized" do
        before(:each) do
          api_authorization_header(user.auth_token)
          delete :destroy, { id: user.id }
        end

        it "should not return any data" do
          expect(response.body.empty?).to be true
        end

        it "should delete user" do
          expect{User.find(user.id)}.to raise_error(/Couldn't find User/)
        end

        it { should respond_with 204 }
      end
    end
  end
end
