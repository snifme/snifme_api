class Api::V1::AccountsController < ApplicationController
  before_action :authenticate_with_token!

  def create
    render Account.create_action_handler(
      user_params,account_params,current_user)
  end


  private

    def account_params
      params.require(:account)
            .permit(:kind,addresses_attributes: [:street1,:street2,:city,:state,
                    :country,:zipcode,:zipcode_plus_4,:latitude,:longitude,
                    :geocoded_number,:geocoded_street,:geocoded_city,
                    :geocoded_country,:geocoded_zipcode,:geocoded_state,
                    :geocoded_state_short,:geocoded_country_short],
                    user_attributes: [:first_name, :last_name])
    end

    def user_params
      params.fetch(:user,{})
            .permit(:first_name,:last_name)
    end


end
