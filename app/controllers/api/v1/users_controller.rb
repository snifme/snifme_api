class Api::V1::UsersController < ApplicationController
  before_action :authenticate_with_token!, only: [:update, :destroy]

  def show
    respond_with User.find(params[:id])
  end

  def create
    user_params.delete id if user_params[:id].present?
    @user = User.new(user_params)
    if @user.valid_password?(user_params[:password]) &&
       @user.save &&
       @user.update_and_send_confirmation_email
      render json: @user, status: 201
    else
      render json: { errors: @user.errors }, status: 422
    end
  end

  def update
    user = current_user

    if user.update(user_params)
      render json: user, status: 200, location: [:api, user]
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def destroy
      current_user.destroy
      head 204
  end

  def confirm_account
    user = User.find(params[:id])
    if user.confirm_account params[:token]
      redirect_to ENV['WEB_URL'] + '/login?confirmed=true'
    else
      redirect_to ENV['WEB_URL'] + '/login?confirmed=false'
    end
  end

  def password_reset_email
    user = User.find_by(email: params[:email])
    if !user
      error = "No user exists with this email"
      render json: { errors: error }, status: 422
    else
      if user.update_password_token_and_send_email
        render json: {}, status: 200
      else
        render json: {}, status: 422
      end
    end
  end

  def password_reset
    user = User.find_by(id: params[:id],reset_password_token: params[:token])
    if user &&
       user.reset_password_sent_at > (Time.now - 1.hour) &&
       user.valid_password?(params[:newPassword])
       user.password = params[:newPassword]
       user.reset_password_token = nil
       if user.save
         render json: {}, status: 200
       else
         render json: { errors: nil }, status: 422
       end
    else
      render json: { errors: "Request Invalid. Password reset token has either already been used, or it has been more than 1 hour since it was sent. Please click \"FORGOT PASSWORD?\" link below to restart password reset process." }, status: 422
    end
  end

  def resend_confirmation
    @user = User.find_by(email: params[:email])

    if !@user
      error = "No user exists with email " + params[:email]
    else
      if @user.confirmed_at.present?
        error = "The account for this email has already been confirmed. Please Login."
      else
        @user.update_and_send_confirmation_email
      end
    end

    if error
      render json: { errors: error }, status: 422
    else
      render json: {}, status: 200
    end
  end

  private

    def user_params
      params.require(:user).permit(:id, :email, :password, :password_confirmation)
    end

end
