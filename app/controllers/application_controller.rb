class ApplicationController < ActionController::Base
  include Authenticable
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session, only: Proc.new { |c| c.request.format =~ %r{application/vnd.snifme.v1}  }
  #respond_to :json


end
