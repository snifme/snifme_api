class UserSerializer < ActiveModel::Serializer
  attributes :id
  has_many :accounts

  class AccountSerializer < ActiveModel::Serializer
    attributes :id,:kind
  end
end
