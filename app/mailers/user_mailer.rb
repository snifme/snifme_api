class UserMailer < ActionMailer::Base
  default from: 'hello@snifme.com'

  def send_confirmation_email(user)
    @user = user
    @link_url = ENV['API_URL'] + '/confirm-account' +
                '?token=' + user.confirmation_token +
                '&id=' + user.id.to_s
    mail( :to => user.email,
    :subject => 'Please confirm your Snifme account!' )
  end

  def send_welcome_email user
    @user = user
    mail({
      to: user.email,
      subject: 'Welcome to Snifme!'
    })
  end

  def send_password_reset_email user
    @user = user
    @link_url = ENV['WEB_URL'] + '/password-reset' +
                '?token=' + user.reset_password_token +
                '&id=' + user.id.to_s
    mail({
      to: user.email,
      subject: 'Snifme Password Reset Request'
    })
  end

end
