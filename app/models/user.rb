class User < ActiveRecord::Base
  include Archiving
  has_secure_password validations: false

  has_many :identities
  has_many :logs
  has_one :facebook
  has_many :accounts

  validates :email, uniqueness: true, presence: true
  validates_length_of :password, :in => 8..20, :on => :create, allow_nil: true

  def is_dog_owner?
    return self.accounts.where(kind: "dog_owner").any?
  end

  def login_and_serialize
    token = Identity.create_with_token(user_id: self.id).token
    return {
      token: token,
      user_email: self.email,
      user_id: self.id,
      first_name: self.first_name,
      last_name: self.last_name
    }
  end

  def update_and_send_confirmation_email
    return false unless self.create_confirmation_token
    UserMailer.send_confirmation_email(self).deliver
    return true
  end

  def valid_password? user_password
    if !user_password ||
       user_password.length < 8
      self.errors.add(:password, "Must be at least 8 characters")
      return false
    end
    return true
  end

  def update_from_facebook data
    if data[:first_name].present?
      self.first_name = data[:first_name] unless self.first_name.present?
    end

    if data[:last_name].present?
      self.last_name = data[:last_name] unless self.last_name.present?
    end

    return self.save
  end

  def create_confirmation_token
    self.generate_confirmation_token
    self.confirmation_sent_at = DateTime.now
    return false unless self.save
    return true
  end

  def confirm_account token
    return false unless confirmation_token == token && confirmed_at.blank?
    self.confirmed_at = Time.now
    if self.save
      UserMailer.send_welcome_email(self).deliver
      return true
    else
      return false
    end
  end

  def update_password_token_and_send_email
    self.update_password_reset_token
    if self.save!
      UserMailer.send_password_reset_email(self).deliver
      return true
    else
      return false
    end
  end

  def serialize_accounts data={}
    accounts = self.accounts.map do |a|
      { account_id: a.id, account_kind: a.kind }
    end
    data[:accounts] = accounts || []
    return data
  end

  protected

  def update_password_reset_token
    self.generate_password_reset_token
   self.reset_password_sent_at = Time.now
  end

  def generate_confirmation_token
    begin
      self.confirmation_token = Devise.friendly_token
    end while self.class.exists?(confirmation_token: confirmation_token)
  end

  def generate_password_reset_token
    begin
      self.reset_password_token = Devise.friendly_token
    end while self.class.exists?(reset_password_token: reset_password_token)
  end

end
