class Session

  def self.create_action_handler params
    user_password = params[:password]
    user_email = params[:email]
    user = user_email.present? && User.find_by(email: user_email)
    return self.authenticate_and_login(user,user_password)
  end

  def self.authenticate_and_login(user,user_password)
    if user &&
       user.password_digest.present? &&
       user.authenticate(user_password)

      response = user.login_and_serialize

      return {json: response, status: 200}
    else
      return {json: { errors: "Invalid email or password. If you registered"+
             " with Facebook and don't yet have a password, please use the"+
             " \"Connect With Facebook\" below to login." }, status: 422}
    end
  end
end
