class Dog < ActiveRecord::Base
  belongs_to :account
  has_many :behaviours
  has_many :dog_events
end
