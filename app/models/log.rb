class Log < ActiveRecord::Base
  belongs_to :user
  validates :category, presence: true
  validates :action, presence: true

  def self.create_session_token
    session_token = self.generate_session_token
    self.create_session_token_log session_token
    session_token
  end

  def self.merge_logs_with_user user_id, session_token
    logs = Log.where(session_token: session_token)
    logs.update_all(session_token: nil, user_id: user_id)
  end

  private

    def self.create_session_token_log session_token
      data = {
        session_token: session_token,
        category: "Account",
        action: "Create",
        label: "Session Token"
      }
      self.create(data)
    end

    def self.generate_session_token
      begin
        session_token = Devise.friendly_token
      end while Log.exists?(session_token: session_token)
      session_token
    end

end
