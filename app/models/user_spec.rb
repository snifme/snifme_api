unless Rails.env.production?
  require 'rails_helper'

  RSpec.describe User, type: :model do

    before { @user = FactoryGirl.build(:user) }

    subject { @user }

    it { should be_valid }

    it { should respond_to(:email) }
    it { should respond_to(:password) }
    it { should respond_to(:password_confirmation) }
    it { should respond_to(:auth_token) }

    # validations
    it { should validate_presence_of(:email) }
    it { should validate_confirmation_of(:password) }
    it { should allow_value('example@domain.com').for(:email) }

    it "email uniqueness validation" do
      FactoryGirl.create(:user, email: "test@gmail.com")
      expect {FactoryGirl.create(:user, email: "test@gmail.com")}.to raise_error(
        /Email has already been taken/
      )
      expect {FactoryGirl.create(:user, email: "TEST@gmail.com")}.to raise_error(
        /Email has already been taken/
      )
    end

    it "auth_token uniqueness validation" do
      first_user = FactoryGirl.create(:user)
      new_user = FactoryGirl.create(:user)
      token = first_user.auth_token
      expect {new_user.update!(auth_token: token)}.to raise_error(
        /Validation failed: Auth token has already been taken/
      )
    end

    describe "#generate_authentication_token!" do
      it "generates a unique token" do
        Devise.stub(:friendly_token).and_return("auniquetoken123")
        @user.generate_authentication_token!
        expect(@user.auth_token).to eql "auniquetoken123"
      end

      it "generates another token when one already has been taken" do
        existing_user = FactoryGirl.create(:user, auth_token: "auniquetoken321")
        @user.generate_authentication_token!
        expect(@user.auth_token).not_to eql existing_user.auth_token
      end
    end
  end
end
