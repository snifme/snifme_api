class Account < ActiveRecord::Base

  belongs_to :user
  has_many :dogs
  belongs_to :establishment
  belongs_to :branch
  has_many :addresses, as: :concernable

  validates_presence_of :user
  validates_presence_of :kind

  enum kind: { dog_owner: 0, establishment_owner: 1, branch_owner: 2,
               customer: 3, employee: 4, manager: 5, partner: 6 }

  accepts_nested_attributes_for :addresses
  accepts_nested_attributes_for :user

  VALID_ACCOUNT_TYPES = ["dog_owner"]

  def self.create_action_handler(user_params, account_params, user)
    account_type = account_params[:kind]

    unless self.account_type_valid? account_type
      return self.error_response("invalid account type")
    end

    if account_type === "dog_owner"
      return self.create_dog_owner(user_params, account_params, user)
    end
  end

  private

    def self.create_dog_owner user_params, account_params, user
      return self.error_response("already dog owner") if user.is_dog_owner?

      user.update(user_params);

      account = user.accounts.build(account_params)

      if account.save
        data = user.serialize_accounts
        return { json: data, status: 201 }
      else
        return self.error_response(account.errors)
      end

    end

    def self.error_response error=nil, status=422
      return { json: { errors: error }, status: status }
    end

    def self.account_type_valid? account_type=nil
      VALID_ACCOUNT_TYPES.include? account_type
    end

end
