class Facebook < ActiveRecord::Base

  belongs_to :user
  validates :user_id, presence: true, uniqueness: true

  def self.prepare_data params
    data = {}
    data[:age_range] = params[:age_range].try(:to_json)
    data[:devices] = params[:devices].try(:to_json)
    data[:email] = params[:email]
    data[:first_name] = params[:first_name]
    data[:gender] = params[:gender]
    data[:is_verified] = params[:is_verified]
    data[:last_name] = params[:last_name]
    data[:link] = params[:link]
    data[:name_format] = params[:name_format]
    data[:timezone] = params[:timezone]
    data[:updated_time] = params[:updated_time]
    data[:verified] = params[:verified]
    data[:cover] = params[:cover].try(:to_json)
    data[:picture] = params[:picture][:data].try(:to_json) if params[:picture]
    data[:uid] = params[:id]
    data[:token] = params[:token]
    return data
  end

  def self.validate_fb_token data
    token = data[:token]
    return false if !token || token.blank?

    base_url = "https://graph.facebook.com/v2.5/me?access_token="
    url = base_url + token + "&fields=email"
    response = HTTParty.get(url)
    return response.present? &&
    data[:uid] == response["id"] &&
    data[:email] == response["email"]
  end

  def self.connect data

    unless self.validate_fb_token data
      return {status: 401, errors: ["Not Authorized"]}
    end

    data.delete(:token)

    if fb = Facebook.find_by(uid: data[:uid])
      # facebook already connected with user
      # log user in
      user = fb.user
      response = user.login_and_serialize
      return {status: 200, json: response }
    elsif user = User.find_by(email: data[:email])

      # user exists but facebook is not connected
      # connect facebook with user and log user in
      return Facebook.create_account(data, user)

    elsif data[:uid].present? && data[:email].present?
      # no user or facebook account exists
      # create user, facebook account and login
      user = User.new(email: data[:email])
      if user.save
        UserMailer.send_welcome_email(user).deliver
        user.update_from_facebook data
        response = Facebook.create_account(data, user)
        unless response[:status] === 422
          response[:json][:user_account_create] = true
        end
        return response
      else
        return {status: 422, errors: user.errors}
      end
    else
      return {status: 401, errors: ["Not Authorized"]}
    end

  end

  def self.create_account data, user
    user.update_from_facebook data
    data[:user_id] = user.id
    data.delete(:token) if data[:token].present?
    fb = Facebook.new(data)
    if fb.save
      response = user.login_and_serialize
      response[:fb_account_create] = true
      return {status: 200, json: response }
    else
      return {status: 422, json: {errors: fb.errors}}
    end
  end

  def self.prepare_data_and_connect params
    data = Facebook.prepare_data params
    Facebook.connect data
  end
end
