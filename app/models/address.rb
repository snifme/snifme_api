class Address < ActiveRecord::Base
  belongs_to :concernable, polymorphic: true
end
