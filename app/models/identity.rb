class Identity < ActiveRecord::Base

  belongs_to :user

  validates :user_id, presence: true

  def self.create_with_token data
    identity = Identity.new(data)
    identity.generate_authentication_token
    identity.save
    return identity
  end

  def generate_authentication_token
    begin
      self.token = Devise.friendly_token
    end while self.class.exists?(token: token)
  end

end
