FROM ruby:2.2.4
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev
RUN apt-get install -y curl vim nodejs postgresql-client-9.4
RUN apt-get install -y openssh-server

# add id_rsa.pub file to authorized keys
RUN mkdir -p /root/.ssh/
ADD id_rsa.pub /root/.ssh/authorized_keys
RUN chmod 600 /root/.ssh/authorized_keys
RUN echo "   Port 22" >> /etc/ssh/ssh_config

RUN echo 'export GEM_PATH=/usr/local' >> $HOME/.bashrc

COPY ./ /root/snifme-api
WORKDIR /root/snifme-api

ENV environment production

# Configure an entry point, so we don't need to specify
# "bundle exec" for each of our commands.

ENTRYPOINT ["bundle", "exec"]

RUN RAILS_ENV=${environment}
RUN /bin/bash -c 'gem install bundle'
RUN /bin/bash -c 'bundle install --without development test'
RUN /bin/bash -c 'RAILS_ENV=${environment} rake assets:precompile'

CMD service ssh restart && rails server -b 0.0.0.0 -e ${environment}

# docker build -t mlennie/snifme-api . && docker stop api1 && docker rm api1 && docker run -d --net mynetwork --net data_network --name api1 -e "environment=production" mlennie/snifme-api && docker network connect data_network api1 && docker stop api2 && docker rm api2 && docker run -d --net mynetwork --name api2 -e "environment=production" mlennie/snifme-api && docker network connect data_network api2
# docker run -it --rm mlennie/snifme-api bash

# postgres
# docker run -d --net mynetwork --name db postgres
# docker run --name db --net data_network -e POSTGRES_PASSWORD=password -d postgres:9.4.5
# rake db:migrate


