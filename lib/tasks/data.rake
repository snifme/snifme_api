namespace :data do

  desc "dump database and gzip"
  task :backup => :environment do
    Postgres.backup
  end

  desc "push db backup to b2"
  task :push_to_b2 => :environment do
    Postgres.push_backup_to_b2
  end

  desc "pull db backup from b2"
  task :pull_from_b2 => :environment do
    Postgres.pull_backup_from_b2
  end

  desc "unzip backup and restore database"
  task :restore => :environment do
    Postgres.restore
  end

  desc "dump database, gzip and push to b2"
  task :backup_and_push => :environment do
    Postgres.backup_and_push_to_b2
  end

  desc "pull, unzip and restore"
  task :pull_and_restore => :environment do
    Postgres.pull_from_b2_and_restore
  end

end
