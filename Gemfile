source 'https://rubygems.org'
ruby '2.2.4'

gem 'dotenv-rails'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.15'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
gem 'turbolinks'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
# Admin Interface
gem 'activeadmin', github: 'activeadmin'
gem 'sanitize_email'

# active admin dependencies:
gem 'cancancan'
gem 'draper'
gem 'pundit'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
gem 'rack-cors', :require => 'rack/cors'

gem 'httparty'

# Api gems
gem 'active_model_serializers'
gem 'devise'

####################
# documentation
####################
gem 'apipie-rails'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Use pry to debug
  gem 'pry-rails'
  gem "shoulda-matchers"
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  gem "rspec-rails", "~> 3"
  gem "factory_girl_rails"
  gem 'ffaker'
  gem 'webmock'
end


